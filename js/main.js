$(document).ready(function(){
	var altura = $('.menu').offset().top;
	$(window).on('scroll',function(){
		if($(window).scrollTop() > altura){
			$('.navbar.navbar-default').removeClass('navbar');	
			$('.navbar-default').addClass('menu-fixed');
			$('.menu-fixed').removeClass('navbar-default');			

		}else{
			$('.menu-fixed').addClass('navbar');
			$('.menu-fixed').addClass('navbar-default');
			$('.navbar-default').removeClass('menu-fixed');

		}
	});
	
});